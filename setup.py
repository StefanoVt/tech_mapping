#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="tech_mapping",
    packages=find_packages(exclude=['tests']),
    install_requires=['pyrbc', 'networkx', 'numpy'],
    test_suite='tests'
)
