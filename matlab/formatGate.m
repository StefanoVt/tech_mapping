function ret = formatGate(name, area, formula, vars)
    templ = 'GATE %s %f %s';
    pintempl = '\n PIN %s INV 0 0 0 0 0 0';
    ret = sprintf(templ, name, area, formula);
    for var = vars
        ret = [ret sprintf(pintempl, var)];
    end
end