classdef CSPBridge < matlab.mixin.Copyable
    properties
    CSP; %actual CSP struct
    placementDetails; %unused, store partial placement details
    A; % topology
    emb; %latest embedding found
    relindex = containers.Map();  %gates library
    end
    methods
        function obj2 = chooseTopology(obj, para)
            %set topology
            %%% use an integer for now
            obj2 = obj.copy();
            if ischar(para)
            else
                obj2.A = getChimeraAdjacency(para);
            end
        end
        function obj2 = addConstraint(obj, gatename, vars)
            % add a constraint to the CSP
            %%% gatename is the name of the relationship in relindex
            %%% vars is the scope in DIMACS format (< 0 means negated)
            obj2 = obj.copy();
            newc.rel = obj.relindex(gatename);
            newc.scope = abs(vars);
            newc.negVars = vars < 0;
            if length(obj.CSP.constraints)
                lastauxscope = max(obj.CSP.constraints(end).auxScope);
                naux = length(obj.CSP.relations(newc.rel).h) - length(vars);
                newc.auxScope = (lastauxscope+1):(lastauxscope+1+naux);
            end
            obj2.CSP.constraints(end+1) = newc;
            %%% place in the middle for now
            place.x = 6;
            place.y = 6;
            place.t = 1;
            place.h = 1;
            place.v = 1;
            obj2.placementDetails{end+1} = place;
            obj2.CSP = CSPUtils.isingModel(obj2.CSP);
        end
        function obj2 = searchEmbedding(obj)
            % update emb
            obj2 = obj;
            if length(obj.CSP.constraints) == 1
                obj.emb = num2cell(num2cell(1:length(obj.CSP.h)));
                return;
            end
            params.pNRFast = false;
            params.maxTries = 10;
            params.hard = true;
                [reducedCSP,scope,auxScope] = CSPUtils.reduceScope(obj.CSP);
            emb = CSPEmbedding(reducedCSP, obj.A, params);
            obj.emb = emb;
            return;
            % the following is the commented-out attempt to use ripandroute
            % with the previously found placement
            % most code is copy-pasted and adapted from CSPEmbedding
            
                [reducedCSP,scope,auxScope] = CSPUtils.reduceScope(obj.CSP);
                reducedCSP = CSPUtils.isingModel(reducedCSP);
            [N,~,L] = getChimeraDims(obj.A);
                fixedVars = false(1,length(reducedCSP.J));
                    cspJ = reducedCSP.J;
                        A4 = reshape(full(obj.A),L,2*N^2,L,2*N^2);
    busGraphEdges = sparse(squeeze(sum(sum(A4,3),1)));

            %N, obj.A
            workingQubits = full(any(obj.A))';
            numWorkingQubits = full(sum(reshape(workingQubits,L,2*N^2)))';
            %gridCapacity = min(reshape(numWorkingQubits,2,N^2))';

            
            opt.busGraphEdges = busGraphEdges;
            %opt.maxChainLength = inf;
            [pemb, obj.placementDetails] = CSPPlaceAndRoute(reducedCSP, obj.placementDetails, N, numWorkingQubits', opt);
                    %% Detailed routing
        if isempty(pemb)
            %assert([]CSPEmbedding(obj.CSP, obj.A));
            obj.emb = [];
            return;
        end
        constraintBusQubits = getConstraintQubits(reducedCSP,N,L,obj.placementDetails);
        [vertexEmbedding,valid] = detailedRoutingCSP(pemb,obj.A,reducedCSP,constraintBusQubits);
        %[vertexEmbedding,valid] = detailedRoutingCSPUnfix(busEmbAll,A,CSP,constraintBusQubits);
        %[vertexEmbedding,valid] = detailedRoutingOrang(busEmbAll,A,trivJ);
        %% check
        if ~verifyEmbedding(cspJ(~fixedVars,~fixedVars),obj.A,vertexEmbedding(~fixedVars));
            error('Here');
        end
        len = cellfun(@length,vertexEmbedding);
            fprintf('Total max chain length = %d, qubits = %d\n',max(len),sum(len));
         
        if ~valid
            %assert([] == CSPEmbedding(obj.CSP, obj.A));
            obj.emb = [];
            return;
        end
        
        %% return to original variables
        scope = [scope, auxScope];
        vertexEmbeddingFull = cell(1,max(scope));
        vertexEmbeddingFull(scope) = vertexEmbedding;
        obj.emb = vertexEmbeddingFull;
        end
        function ret = get_area(obj)
            ret = 0;
            for i=obj.emb
                ret = ret + length(i{1});
            end
        end
        function obj = loadBLIF(obj, fileName)
            obj.CSP = blifToCSP(fileName);
        end
        function obj = loadRelmap(obj, fileName)
            relmap = genlibToRelations(fileName);
            relmap.remove('not');
            relkeys = relmap.keys();
            obj.CSP = CSPUtils.emptyCSP(length(relkeys),0);
            for i=1:length(relkeys)
                rel = relmap(relkeys{i}); 
                rel.en = rel.en0;
                rel = rmfield(rel, 'en0');
                obj.CSP.relations(i) = rel;
                obj.relindex(relkeys{i}) = i;
            end
              
        end
        
        function emb = getEmbedding(obj)
            emb = savejson(obj.emb);
        end
        
        function score = tryPlacement(obj)
            A = obj.A;
            if length(obj.CSP.constraints) < 2
                score = 0;
                return;
            end
            CSP = CSPUtils.reduceScope(obj.CSP);
                [N,~,L] = getChimeraDims(A);
    %N = sqrt(length(A)/(2*L));
    workingQubits = full(any(A))';
    numWorkingQubits = full(sum(reshape(workingQubits,L,2*N^2)))';
    gridCapacity = min(reshape(numWorkingQubits,2,N^2))';
                A4 = reshape(full(obj.A),L,2*N^2,L,2*N^2);
    busGraphEdges = sparse(squeeze(sum(sum(A4,3),1)));
        [F,incMat,auxMat] = CSPUtils.factorGraph(CSP);
    incMat = [incMat,auxMat];
    [numFactors,~] = size(incMat);
        factorSizes = zeros(numFactors,3);
    for i = 1:numFactors
        factorSizes(i,:) = CSP.relations(CSP.constraints(i).rel).dims;
    end
    gridGraph = sparse(grid_graph(N,N) - eye(N^2));
    % incorporate missing edges into capacity
    idx = 2:2:2*N^2;
    intraEdges = busGraphEdges(2*N^2*(idx-2)+idx)';
    gridCapacity = min(gridCapacity,full(floor(sqrt(intraEdges))));
            [placement,~] = planarPlacement(F,[N,N],gridCapacity,factorSizes,false);
            if isempty(placement)
                score = inf;
            else
                score = placementScore(incMat,gridGraph,placement);
            end
        end
    end
end