from itertools import combinations
from unittest import TestCase
from mock import Mock
from tech_mapping.matching import GateMatch, Cut
from tech_mapping.relations import Function
from tech_mapping.ising import IsingModel
from pyrbc.rbc import var, And, Or
import numpy as np

def old_to_hJ(mat):
    h = list(mat.diagonal())
    J = {k:mat[k] for k in combinations(range(len(h)),2) if mat[k] != 0}
    return h,J

class TestGateMatch(TestCase):
    def test1(self):
        a = var("a")
        b = var("b")
        c = var("c")
        d = var('d')

        function = Function(And(a,b), ['a', 'b'])
        arr, off =  np.array([[0.25,0,-0.5,-0.75],[0,-0.25,0.25,-0.5],[0,0,-0.25,0],[0,0,0,0.25]]),1.5
        h,J = old_to_hJ(arr)
        isingm = IsingModel("and2", function,h,J, off)
        gm = GateMatch(Cut(Or(c,~d), [c.node,d.node]), isingm, Function(Or(c,~d), ['c','d']))
        blifline = gm.get_blif_gate(map(lambda x:x.node, [a,b,c,d,And(a,b), And(~c,d)]))
        self.assertEqual(blifline, ".gate and2 A=n5 C=n2 B=n3\n")
        gm.verify()

    def test_incorrect(self):
        a = var("a")
        b = var("b")
        c = var("c")
        d = var('d')

        function = Function(And(a, b), ['a', 'b'])
        arr, off = np.array([[0.25, 0, -0.5, -0.75], [0, -0.25, 0.25, -0.5], [0, 0, -0.25, 0], [0, 0, 0, 0.25]]), 1.5
        h,J = old_to_hJ(arr)
        isingm = IsingModel("and2", function, h,J, off)
        gm = GateMatch(Cut(Or(And(c,d),And(~c,~d)), [c.node, d.node]), isingm, Function(Or(And(c,d),And(~c,~d)), ['c', 'd']))
        self.assertRaises(AssertionError, gm.verify)
