from os.path import dirname
from unittest import TestCase

from pyrbc.aiger import parse_aig
from pyrbc.cnf import parse_cnf, cnf_to_rbc
from pyrbc.rbc import var, And
from tech_mapping.embedding import GateNetwork


class TestEmbedding(TestCase):
    def test_empty(self):
        e = GateNetwork([], {})
        self.assertEqual(e.vars, [])

    def test_single(self):
        pass

    def test_multi(self):
        pass