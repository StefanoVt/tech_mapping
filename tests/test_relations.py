from unittest import TestCase

from pyrbc.rbc import var, And, Or
from tech_mapping.relations import Function


class TestRelation(TestCase):
    def test_from_aig(self):
        r = Function(And(var('a'), var('b')), ['a', 'b'])

        self.assertTrue(r)

    def test_find_canonical(self):
        r = Function(And(var('a'), var('b')), ['a', 'b'])
        r = r.find_canonical()
        r2 = Function(Or(var('a'), var('b')), ['a', 'b'])
        r2 = r2.find_canonical()

        self.assertEqual(r, r2)


    def test_get_key(self):
        r = Function(And(var('a'), var('b')), ['a', 'b'])
        self.assertTrue(r.get_key() and hash(r.get_key()))

