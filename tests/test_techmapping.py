from __future__ import print_function
from unittest import TestCase, skip
from os.path import dirname
from dimod import SimulatedAnnealingSampler
from tech_mapping.read_genlib import read_genlib,create_relation_db
from tech_mapping.mapper.bottomup import BottomUpMapper
from tech_mapping.mapper.topdown import TopDownMapper
from tech_mapping.embedding import GateNetwork, EmbeddingTopology
from chimerautils.chimera import create
from pyrbc.aiger import parse_aig
from itertools import combinations
import numpy

def get_chimera_adjacency(n):
    return create(n,n)[0]

class TestMapper2(TestCase):
    def test_small(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        emb = BottomUpMapper(gdb, GateNetwork, create(12, 12)[0])
        with open(dirname(__file__) + '/../data/74283.isc.aig') as f:
            g = parse_aig(f)
        finalemb = emb.mapping(g)
        print(finalemb.size, finalemb.gates)
        assert finalemb
        print(finalemb.get_blif())

        et = EmbeddingTopology(get_chimera_adjacency(8),finalemb)
        h, J, off = (et.expand_embedding())
        h_, J_, off_ = finalemb.unconstrained_ising()



        sols = SimulatedAnnealingSampler().sample_ising(h,J)
        #print(off)
        #print(sols)
        for vsol, osol in zip(sols,et.unembed_solution(sols)):
            res = off
            for x in range(len(h)):
                res += h[x] * vsol[x]

            for x, y in J:
                res += J[x, y] * vsol[x] * vsol[y]

            res_ = off_
            for x in range(len(h_)):
                res_ += h_[x] * osol[x]

            for x,y in J_:
                res_ += J_[x,y] * osol[x] * osol[y]
            print(res, res_, osol)
        #for gate in finalemb.gates:
        #    gate.verify()
        finalemb.verify()
        #finalemb.show_embedding()

    def test_complex_matching(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        emb = BottomUpMapper(gdb, GateNetwork, create(12, 12)[0])
        with open(dirname(__file__) + '/../data/74L85.isc.aig') as f:
            g = parse_aig(f)
        finalemb = emb.mapping(g)
        print(finalemb.size, finalemb.gates)
        assert finalemb
        print(finalemb.get_blif())
        finalemb.show_embedding()
        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.verify()

    def test_complex3(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        emb = BottomUpMapper(gdb, GateNetwork, create(12, 12)[0])
        with open(dirname(__file__) + '/../data/c499_red.aig') as f:
            g = parse_aig(f)

        finalemb = emb.mapping(g)
        print(finalemb.size)
        assert finalemb
        finalemb.show_embedding()
        print(finalemb.get_blif())
        for gate in finalemb.gates:
            gate.verify()


@skip("not fixed yet")
class TestDatasets(TestCase):
    def test_small(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        with open(dirname(__file__) + '/../../datasets/74x/74182.isc.aig') as f:
            g = parse_aig(f)
        emb = TopDownMapper(gdb, GateNetwork, create(12, 12)[0])
        final_emb = emb.mapping(g)
        print(final_emb.size, final_emb.gates, final_emb.embedding)
        assert final_emb
        print(final_emb.get_blif())

        for gate in final_emb.gates:
            gate.verify()
        final_emb.verify()
        final_emb.show_embedding()

    def test_complex_matching(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        emb = TopDownMapper(gdb, GateNetwork, get_chimera_adjacency(12))
        with open(dirname(__file__) + '/../data/74L85.isc.aig') as f:
            g = parse_aig(f)

        finalemb = emb.mapping(g)
        print (finalemb.size, finalemb.gates, finalemb.embedding)
        assert finalemb
        print (finalemb.get_blif())
        finalemb.show_embedding()
        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.verify()

    def test_complex3(self):
        import sys
        sys.setrecursionlimit(1000000)
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)

        gdb = create_relation_db(res)

        emb = TopDownMapper(gdb, GateNetwork, get_chimera_adjacency(12))
        with open(dirname(__file__) + '/../../datasets/iscasaig/c499.aig') as f:
            g = parse_aig(f)

        finalemb = emb.mapping(g)
        print (finalemb.size, finalemb.embedding)
        assert finalemb
        finalemb.show_embedding()
        print (finalemb.get_blif())
        for gate in finalemb.gates:
            gate.verify()


@skip("old to be reviewed")
class TestEmbedding(TestCase):
    def test_creation(self):
        emb = GateNetwork(SimpleGateDatabase(), get_chimera_adjacency(12))
        g = And(var("a"), var("b"))
        # assign_ids(g)
        finalemb = emb.mapping(g)
        assert finalemb
        finalemb.verify()

    def test_complex(self):
        emb = GateNetwork(SimpleGateDatabase(), get_chimera_adjacency(12))
        with open(dirname(__file__) + '/../data/c499_red.cnf') as f:
            g = parse_cnf(f)
            g = cnf_to_rbc(g[:20])
        #assign_ids(g)
        finalemb = emb.mapping([g])
        assert finalemb
        for gate in finalemb.gates:
            gate.verify()
        finalemb.verify()

    def test_complex2(self):
        emb = GateNetwork(SimpleGateDatabase(), get_chimera_adjacency(12))
        with open(dirname(__file__) + '/../data/74L85.isc.aig') as f:
            g = parse_aig(f)
        g = reduce(And, g)
        # assign_ids(g, clear=True)
        finalemb = emb.mapping(g)
        print (finalemb.size, finalemb.embedding)
        assert finalemb
        #finalemb.show_embedding()
        for gate in finalemb.gates:
            gate.verify()
        finalemb.verify()

    def test_complex_matching(self):
        import sys
        sys.setrecursionlimit(1000000)
        gdb = SimpleGateDatabase()

        emb = TopDownMapper(gdb, GateNetwork, get_chimera_adjacency(6))
        with open(dirname(__file__) + '/../../datasets/74x/74283.aig') as f:
            g = parse_aig(f)
        #plot(g[0].node.vertex.graph, vertex_size=5)
        finalemb = emb.mapping(g)

        print (finalemb.size, finalemb.gates, finalemb.embedding)
        assert finalemb
        print (finalemb.get_blif())
        finalemb.show_embedding()
        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.verify()
