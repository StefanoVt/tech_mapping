from cProfile import Profile
from os.path import dirname
from pstats import Stats
from unittest import TestCase

from pyrbc.imports import parse_aig
from tech_mapping.embedding import EmbeddingMatlab, Embedding2
from tech_mapping.mapper.bottomup import BottomUpMapper
from tech_mapping.gate_db import GateDatabase
from tech_mapping.read_genlib import create_relation_db, read_genlib

from unittest import TestCase,skip

import numpy

from pyrbc.rbc import var, And
from tech_mapping.matlabridge import MatlabRelation
import os, tech_mapping.matlabridge


@skip("see test_relations.py for updateinterface")
class TestMatlabRelation(TestCase):
    def test_from_aig(self):
        r = MatlabRelation.from_aig(And(var('a'), var('b')), ['a', 'b'])

        self.assert_(r)

    def test_find_canonical(self):
        r = MatlabRelation.from_aig(And(var('a'), var('b')), ['a', 'b'])
        r, _, _ = r[0].find_canonical()

        self.assertEqual(r.get_key(), numpy.array([[0, 0, 0],
                                                   [0, 0, 1],
                                                   [0, 1, 0],
                                                   [1, 1, 1]]).tobytes())


    def test_get_key(self):
        r = MatlabRelation.from_aig(And(var('a'), var('b')), ['a', 'b'])
        self.assert_(r[0].get_key() and hash(r[0].get_key()))


class TestMatlab(TestCase):
    def test_small(self):
        reldb = "../../datasets/generated2.genlib"
        EmbeddingMatlab.create_relmap(reldb)
        with open(reldb) as f:
            db = create_relation_db(read_genlib(f))
        emb = BottomUpMapper(db, Embedding2, 12)
        with open(dirname(__file__) + '/../../datasets/74x/74182.isc.aig') as f:
            g = parse_aig(f)
        finalemb = emb.mapping(g)
        print finalemb.size, finalemb.gates, finalemb.embedding
        print finalemb
        print finalemb.get_blif()

        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.show_embedding()
        finalemb.verify()

    def test_creation3(self):
        reldb = "../../datasets/generated2.genlib"
        EmbeddingMatlab.create_relmap(reldb)
        with open(reldb) as f:
            rdb = create_relation_db(read_genlib(f))
        db = GateDatabase(rdb)
        emb = BottomUpMapper(db, Embedding2, 12)
        with open(dirname(__file__) + '/../../datasets/74x/74L85_faulty.aig') as f:
            g = parse_aig(f)
        finalemb = emb.mapping(g)
        print finalemb.size, finalemb.gates, finalemb.embedding
        print finalemb
        print finalemb.get_blif()

        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.show_embedding()
        finalemb.verify()

    def test_creation2(self):
        reldb = "/home/svarotti/projects/datasets/generated2.genlib"
        EmbeddingMatlab.create_relmap(reldb)
        with open(reldb) as f:
            db = create_relation_db(read_genlib(f))
        emb = BottomUpMapper(db, Embedding2, 16)
        with open(dirname(__file__) + '/../../datasets/iscasaig/c499.aig') as f:
            g = parse_aig(f)
        profile = Profile()
        profile.enable()
        finalemb = emb.mapping(g)
        profile.disable()
        Stats(profile).print_stats()
        print finalemb.size, finalemb.gates, finalemb.embedding
        print finalemb
        print finalemb.get_blif()

        # for gate in finalemb.gates:
        #    gate.verify()
        finalemb.show_embedding()
        finalemb.verify()
