from __future__ import print_function
from os.path import dirname
from unittest import TestCase, skip


from pyrbc.rbc import *
from tech_mapping.read_genlib import read_genlib, create_relation_db


class TestRead_genlib(TestCase):
    def test_read_genlib(self):
        with open(dirname(__file__) + '/../data/generated2.genlib') as f:
            res = read_genlib(f)
        print(res)
        self.assertTrue(len(res) > 0)

        gdb = create_relation_db(res)
        for k, v in gdb.database.items():
            v.verify()
        v = And(var('a'), var('b'))

        res = gdb.find_gate(v, list(map(lambda n: var(n).node, ['a', 'b'])))
        res.verify()
        self.assertEqual(res.area, 4)

