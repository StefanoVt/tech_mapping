from __future__ import division
from unittest import TestCase
from tech_mapping.ising import IsingModel
from tech_mapping.relations import Function
from pyrbc.rbc import And, var, Or
from fractions import  Fraction
import numpy as np

class IsingModelTest(TestCase):
    def test_wrong_values(self):
        im = IsingModel(Function(And(var('a'),var('b')), ['a', 'b']), [1, 1, 1],{} , 0)
        with self.assertRaises(AssertionError) as ctx:
            im.verify()
        self.assertTrue(ctx)

    def test_coherent(self):
        im = IsingModel( Function(Or(Or(And(var('a'), var('b')),And(var('a'), var('c'))),And(var('b'),var('c'))),
                                        ['a', 'b', 'c']),
                        [0,0,0,0],
                        {(0,1):-1,(0,2):-1,(0,3):-1,(1,2):1/2,(1,3):1/2,(2,3):1/2},
                        3/2)
        im.verify()

    def test_permute(self):
        im = IsingModel(Function(Or(Or(And(var('a'), var('b')),And(var('a'), var('c'))),And(var('b'),var('c'))),
                                        ['a', 'b', 'c']),
                        [0, 0, 0, 0],
                        {(0, 1): -1, (0, 2): -1, (0, 3): -1, (1, 2): 1 / 2, (1, 3): 1 / 2, (2, 3): 1 / 2},
                        3/2)
        im.permute([0,1,3,2])
        im.verify()

    def test_negate(self):
        im = IsingModel(
                        Function(Or(Or(And(var('a'), var('b')), And(var('a'), var('c'))), And(var('b'), var('c'))),
                                 ['a', 'b', 'c']),
                        [0, 0, 0, 0],
                        {(0, 1): -1, (0, 2): -1, (0, 3): -1, (1, 2): 1 / 2, (1, 3): 1 / 2, (2, 3): 1 / 2},
                        3 / 2)
        im.toggle(0)
        im.verify()
        im.toggle(3)
        im.verify()

    def test_negpermute(self):
        im = IsingModel(
                        Function(Or(Or(And(var('a'), var('b')), And(var('a'), var('c'))), And(var('b'), var('c'))),
                                 ['a', 'b', 'c']),
                        [0, 0, 0, 0],
                        {(0, 1): -1, (0, 2): -1, (0, 3): -1, (1, 2): 1 / 2, (1, 3): 1 / 2, (2, 3): 1 / 2},
                        3 / 2)
        im.toggle(0)
        im.toggle(3)
        im.verify()
        im.permute([0,3,1,2])
        im.verify()

    def test_match(self):
        im = IsingModel(
                        Function(Or(Or(And(var('a'), var('b')), And(var('a'), var('c'))), And(var('b'), var('c'))),
                                 ['a', 'b', 'c']),
                        [0, 0, 0, 0],
                        {(0, 1): -1, (0, 2): -1, (0, 3): -1, (1, 2): 1 / 2, (1, 3): 1 / 2, (2, 3): 1 / 2},
                        3 / 2)
        im.toggle(0)
        im.toggle(3)
        im.permute([0, 3, 1, 2])
        im2 = im.match(Function(Or(Or(And(var('a'), var('b')), And(var('a'), var('c'))), And(var('b'), var('c'))),
                                 ['a', 'b', 'c']))
        im2.verify()

    def test_weird(self):
        im = IsingModel(
                        Function(Or(Or(And(var('a'), var('b')), And(var('a'), var('c'))), And(var('b'), var('c'))),
                                 ['a', 'b', 'c']),
                        [0, 0, 0, 0],
                        {(0, 1): -1, (0, 2): -1, (0, 3): -1, (1, 2): 1 / 2, (1, 3): 1 / 2, (2, 3): 1 / 2},
                        3 / 2)
        im2 = im.match(im.relation)
        im2.verify()