#!/bin/sh
ABCPATH="$HOME/projects/abc"
SOURCEDIR="$HOME/projects/datasets/iscasbench"
DESTDIR="/tmp/"
for i in $SOURCEDIR/*.bench; do
DESTFILE="$DESTDIR/$(basename ${i%.bench})"
 $ABCPATH/abc -f $ABCPATH/abc.rc -c "read $i;strash" -f ./simplify.abc -c "write ${DESTFILE}.aig" -f ./convert.abc -c "write ${DESTFILE}.blif"
done
