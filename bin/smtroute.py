"""Try to use SMT to do place and route, does not work"""
import sys
from collections import Counter, namedtuple
from itertools import combinations, groupby
from math import copysign

from dwave_sapi2.util import get_chimera_adjacency
from igraph import Graph, plot
from pysmt.shortcuts import *
from pysmt.typing import INT, FunctionType

sys.path.append('/home/svarotti/.smt_solvers/python_bindings/')
reset_env()
env = get_env()
env.factory._get_available_solvers()

G = Graph()
CH = Graph()


def sign(x):
    return copysign(1, x)


def parse_cnf(f):
    ret = []
    for line in f:
        if line.startswith("p") or line.startswith('c'):
            continue
        lits0 = line.strip().split()
        ret.append(tuple(int(x) for x in lits0[:-1]))
    return ret


def num_vars(cnf):
    return max(max(abs(l) for l in cl) for cl in cnf)


def sign(x):
    return copysign(1, x)


def parse_cnf(f):
    ret = []
    for line in f:
        if line.startswith("p") or line.startswith('c'):
            continue
        lits0 = line.strip().split()
        ret.append(tuple(int(x) for x in lits0[:-1]))
    return ret


def num_vars(cnf):
    return max(max(abs(l) for l in cl) for cl in cnf)


def clause_to_hubo(cl):
    ret = []
    ret.append((1, set()))
    ret.append((sign(cl[0]), set([abs(cl[0])])))
    for l in cl[1:]:
        newret = []
        for coeff, terms in ret:
            newret.append((coeff * sign(l), terms ^ set([abs(l)])))
        ret.extend(newret)

    return [(sum(a[0] for a in g), k) for k, g in groupby(ret, lambda x: x[1])]


iff = namedtuple('iff', ('f', 'x1', 'x2'))


def decrease_width(clauses):
    global lastx1, lastx2
    nvars = num_vars(clauses)

    scoreboard = Counter()

    for cl in clauses:
        if type(cl) == iff: continue
        if len(cl) < 3: continue
        for cnt in combinations(cl, 2):
            scoreboard.update([cnt])

    winner = scoreboard.most_common(1)[0][0]
    newvar = nvars + 1
    ret = [iff(newvar, winner[0], winner[1])]
    for cl in clauses:
        if type(cl) == iff:
            ret.append(cl)
        elif all(x in cl for x in winner):
            ret.append((newvar, ) + tuple(x for x in cl if x not in winner))
        else:
            ret.append(cl)

    return ret


def cnf_to_qubo(clauses):
    clauses = list(clauses)
    while max(len(x) for x in clauses if type(x) != iff) > 2:
        clauses = decrease_width(clauses)
    ret = []
    for cl in clauses:
        if type(cl) == iff:
            c, a, b = cl
            ret.extend([
                (0.5 * 4, set()),
                (0.5 * 1 * sign(a), set([abs(a)])),
                (0.5 * 2 * sign(b), set([abs(b)])),
                (0.5 * -3 * sign(c), set([abs(c)])),
                (0.5 * -2 * sign(a) * sign(c), set([abs(a), abs(c)])),
                (0.5 * -3 * sign(b) * sign(c), set([abs(b), abs(c)])),
                (0.5 * 1 * sign(a) * sign(b), set([abs(a), abs(b)])),
            ])  #XXX
        elif len(cl) < 2:
            ret.extend([(1, set()), (-1 * sign(cl[0]), set([abs(cl[0])]))])
        else:
            a, b = cl
            ret.extend([
                (0.5 * 1, set()), (0.5 * -1 * sign(a), set([abs(a)])),
                (0.5 * -1 * sign(b), set([abs(b)])),
                (0.5 * 1 * sign(a) * sign(b), set([abs(a), abs(b)]))
            ])
    retdict = dict()
    for c, term in ret:
        term = frozenset(term)
        if term not in retdict:
            retdict[term] = c
        else:
            retdict[term] += c
    return [(b, a) for a, b in retdict.items()]


with open("/home/svarotti/Downloads/" +
          "sc14-crafted/edges-024-4-5667555-1-00.cnf") as f:
    clauses = parse_cnf(f)
assert all(len(x) >= 2 for x in clauses)
qubo = cnf_to_qubo((clauses))
print num_vars(clauses), len(clauses), max(max(x[1]) for x in qubo
                                           if x[1]), len(qubo)

edges = [tuple(x[1]) for x in qubo if len(x[1]) == 2]
G = Graph.TupleList(edges)
plot(G)

CH = Graph.TupleList((get_chimera_adjacency(1, 1, 4)))
plot(CH)

embs = Symbol("embedding", FunctionType(INT, (INT, )))


def emb(x):
    return Function(embs, ((x), ))


touchfunc = Symbol("touching", FunctionType(INT, (INT,INT)))
def dotouch(a,b):
    return Function(touchfunc, (a,b))

ftouchfunc = Symbol("firsttouch", FunctionType(INT, (INT,)))
def firsttouch(a):
    return Function(ftouchfunc, (a,))
stouchfunc = Symbol("secondtouch", FunctionType(INT, (INT,)))
def secondtouch(a):
    return Function(stouchfunc, (a,))


totedges = []
#proxyvar = len(G.vs) * len(CH.vs) + 100
for a, b in G.get_edgelist():
    f = dotouch(Int(a),Int(b))
    c1 = Or( And(Equals(emb(firsttouch(f)), Int(a)), Equals(emb(secondtouch(f)), Int(b))),
    And(Equals(emb(firsttouch(f)), Int(b)), Equals(emb(secondtouch(f)), Int(a))))
    c2 = GE(f, Int(0))
    c3 = LT(f, Int(len(CH.es)))
    totedges.extend([c1,c2,c3])

for i, edge  in enumerate(CH.get_edgelist()):
    p, q = edge
    totedges.append(Equals(firsttouch(Int(i)), Int(p)))
    totedges.append(Equals(secondtouch(Int(i)), Int(q)))


connfunc = Symbol("connected", FunctionType(INT, (INT, )))


def conn(x):
    return Function(connfunc, (Int(x), ))


embtoconnmap = Symbol("embtoconnmap", FunctionType(INT, (INT, )))


def embtoconn(x):
    return Function(embtoconnmap, (x, ))

def embi(x):
    return emb(Int(x.index))

totcl = []
for n in CH.vs:
    #conn(n) = max([n.index]=[conn(v)) for v in n.neigh() if emb(v) ==emb(n)])
    defconn = Equals(
        conn(n.index), Max([Int(n.index)] + [Ite(
            Equals(embi(n), embi(v)), conn(v.index), Int(0))
                                             for v in n.neighbors()]))
    assconn = Equals(embtoconn(embi(n)), conn(n.index))
    bounds = [
        GE(embi(n), Int(0)), LE(embi(n), Int(len(G.vs))),
        GE(conn(n.index), Int(0)), LE(conn(n.index), Int(len(CH.vs)))
    ]
    totcl.append(defconn)
    totcl.append(assconn)
    totcl.append(And(bounds))

print len(totedges), len(totcl)  #, len(card)
formula = (And(totedges + totcl))
print get_formula_size(formula)
from pysmt.logics import QF_UFLIA
env.factory.add_generic_solver("omsat", "/tmp/optimathsat-1.4.0-linux-64-bit/bin/optimathsat", [QF_UFLIA])
# with Solver(name="z3") as s:
#    get_env().enable_infix_notation = True
#    print s.solve(And(totedges + totcl))
from pysmt.shortcuts import is_sat

is_sat(And(totedges + totcl))
