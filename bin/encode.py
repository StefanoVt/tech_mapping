import argparse
from os import system
import tempfile
import placeandroute.tilebased.parallel as parheur
from multiprocessing import Pool
from dwave_networkx.generators.chimera import chimera_graph
from dwave_networkx.generators.pegasus import pegasus_graph
from tech_mapping.read_genlib import read_genlib

hardware= {
    "chimera8": chimera_graph(8),
    "chimera12": chimera_graph(12),
    "chimera16": chimera_graph(16),
    "pegasus6" : pegasus_graph(6),
    "pegasus8": pegasus_graph(8),
    "pegasus12": pegasus_graph(12),

}

parser = argparse.ArgumentParser(description='''Encode a SAT problem into a QA model.''')

parser.add_argument('problem', help=("input SAT problem as AIG file"))
parser.add_argument('library',  help="pre-encoded librari as GENLIB file")
parser.add_argument('--hardware', '-g', default="chimera16", choices=hardware.keys(),
                    help="QA hardware (default: chimera16)")


args = parser.parse_args()


with open(args.library) as lf:
    library = read_genlib(lf)

hardware_graph = hardware[args.hardware]

#encode and techmap with abc
with tempfile.gettempdir() as dir:
    abc_script = dir + "/techmap.abc"
    blif_out = dir = "/output.blif"
    with open(abc_script, "w") as abc:
        print("read_aig {}".format(args.problem), file=abc)
        print("read_genlib {}".format(args.library), file=abc)
        print("source simplify.abc", file=abc)
        print("source convert.abc", file=abc)
        print("write_blif {}".format(blif_out), file=abc)
    system("abc {}".format(abc_script))
    mapped_file = open(blif_out)


#parse blif andpnr
mapped = parse_blif(mapped_file)
simpl_graph, choices = xxx(hardware_graph)
placer = parheur.ParallelPlacementHeuristic(mapped, simpl_graph,choices)
thread_pool = Pool()
placer.par_run(thread_pool)
chains = placer.chains

#build final graph and output