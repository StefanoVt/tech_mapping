#!/usr/bin/env python2
"""Run mapper on all example circuits"""
import glob
import sys
from os.path import dirname

from dwave_sapi2.util import get_chimera_adjacency

from pyrbc.imports import parse_aig
from tech_mapping.embedding import GateNetwork, EmbeddingMatlab, Embedding2
from tech_mapping.mapper.topdown import TopDownMapper
from tech_mapping.mapper.bottomup import BottomUpMapper
from tech_mapping.gate_db import GateDatabase
from tech_mapping.read_genlib import read_genlib, create_relation_db

sys.setrecursionlimit(1000000)

with open(dirname(__file__) + '/../data/generated2.genlib') as f:
    res = read_genlib(f)

gdb = create_relation_db(res)

for model in gdb.database.values():
    model.verify()


def main(fn):
    print fn
    with open(fn) as f:
        g = parse_aig(f)
    try_to_embed(g)


def try_to_embed(g):
    emb = TopDownMapper(gdb, GateNetwork, get_chimera_adjacency(12))
    finalemb = emb.mapping(g)
    print finalemb.size, finalemb.embedding
    # assert finalemb
    # finalemb.show_embedding()
    print finalemb.get_blif()
    # for gate in finalemb.gates:
    #    gate.verify()


def try_to_embedMatlab(g):
    emb = TopDownMapper(gdb, EmbeddingMatlab, (12))
    finalemb = emb.mapping(g)
    print finalemb.size, finalemb.embedding
    # assert finalemb
    # finalemb.show_embedding()
    print finalemb.get_blif()
    # for gate in finalemb.gates:
    #    gate.verify()


def try_to_embed2(g):
    emb = BottomUpMapper(gdb, Embedding2, 12)
    finalemb = emb.mapping(g)
    print finalemb.size, finalemb.embedding
    # assert finalemb
    # finalemb.show_embedding()
    print finalemb.get_blif()
    # for gate in finalemb.gates:
    #    gate.verify()


fn = dirname(__file__) + '/../../datasets/iscasaig/c499_faulty.aig'
if __name__ == '__main__':
    for fn in glob.glob(dirname(__file__) + '/../../datasets/74x/*_faulty.aig'):
        main(fn)
