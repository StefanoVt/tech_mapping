"""Classes for Boolean matching"""
from itertools import product

from pyrbc.nodes.base import RBCNode

from tech_mapping.relations import Function
from tech_mapping.ising import IsingModel, IsingModelWrapper
from typing import List


class Cut(object):
    def __init__(self, output, inputs):
        self.output = output
        self.inputs = inputs

    def eval(self,assgm):
        assert all(x in self.inputs for x in assgm.keys())
        ret = self.output.eval(assgm)
        return ret

    def set_cutnodes(self,cn):
        self.cut_nodes = cn

class GateMatch(object):
    """Match between a cut and a gate (ising model) from the library"""
    def __init__(self, cut, model, function):
        # type: (Cut, IsingModel, Function) -> None
        self.cut = cut
        self.model_wrap = IsingModelWrapper(model, function)


    @property
    def area(self):
        """Number of qubits in the Ising model"""
        return (self.model_wrap.num_qubits())
        # return self.cutsize

    @property
    def ising_model(self):
        return self.model_wrap.get()

    def to_matlab_constraint(self, vars):
        """Convert match to a matlab compatible format

        actually what is returned is the name of the gate and the matched nodes in the correct order, the caller
        has the responsiblity to build the struct. Exploiting NPN equivalence of penalty functions, all AIG nodes
        can be matched to a qubit without caring for the polarity, that is factored in the relation matrix."""

        model = self.model_wrap.get()
        return model.gatename, model.varnames

    def get_blif_gate(self, vars):
        # type: (List[RBCNode]) -> str
        """Convert match to BLIF gate specification

        TODO: this currently ignores variable polarity for gates, it is not important for minor embedding and the
        parameter setting code in embedding.py is correct but the generated BLIF lacks the correct NOT gates"""
        model = self.model_wrap.get()

        getindex = vars.index

        wires = zip(model.varnames, map(getindex, [self.cut.output.node] + self.cut.inputs))
        return ".gate {} {}\n".format(model.gatename, " ".join("{}=n{}".format(*w) for w in wires))

    def verify(self):
        """Verify that the cut and the gate correctly match"""
        model = self.model_wrap.get()
        model.verify()
        for assgn in product(*([(False, True)] * len(self.cut.inputs))):
            assgnc = dict(zip(self.cut.inputs, assgn))
            assgnr = dict(zip(model.relation.inputs, assgn))
            c = self.cut.eval(assgnc)
            r =  model.relation.aig.eval(assgnr)
            assert c == r, (c,r, assgn)
            del assgnc, assgnr


