"""Read gate library in genlib format

Standard genlib obviously does not containg Ising model information; it is added as a comment in the same line a gate
is specified. this comment is a JSON object with the converted matlab struct"""
import json
from itertools import combinations

import re

import numpy

from tech_mapping.ising import IsingModel
from tech_mapping.relations import Function, FunctionCertificate
from tech_mapping.gate_db import GateDatabase
from typing import Dict, BinaryIO
# GATE (name) (area) (formula);#(JSONData)\n
gate_regex = re.compile(r'GATE (\w+) ([0-9.]+) ([^;]+);(?:#(.+))\n')


def read_genlib(f):
    # type: (TextIO) -> Dict[str, Dict]
    """Read genlib file into a dict object"""
    ret = dict()
    for l in f.readlines():
        if l.startswith("#"):
            continue
        m = gate_regex.match(l)
        if not m:
            continue
        name, area, form, jso = m.groups()
        data = json.loads(jso)
        data["area"] = float(area)
        data["form"] = form
        ret[name] = data
    return ret


def create_relation_db(genlib_db):
    # type: (Dict[str, Dict]) -> GateDatabase
    """Create a relation database from a dictionary of gates"""
    newdb = dict()
    for name, data in genlib_db.items():
        crel, isingmodel = parse_old_format(data, name)

        if crel.get_key() in newdb:
            oldmodel = newdb[crel.get_key()]
            if isingmodel.num_ancilla < oldmodel.num_ancilla:
                newdb[crel.get_key()] = isingmodel
        newdb[crel.get_key()] = isingmodel

    return GateDatabase(newdb)



def parse_old_format(data, name):
    relmap = numpy.array(data["R"])
    isingmat = numpy.array(data["J"])
    h = numpy.array(data["h"]).reshape((len(data['h']),))
    for i in range(len(h)):
        isingmat[i, i] = h[i]
    vP = [x[0] - 1 for x in data["vP"][0]]
    vP += [x for x in range(int(data["area"])) if x not in vP]
    uvp = list(range(len(vP)))
    for i, v in enumerate(vP):
        uvp[v] = i
    M = isingmat
    M[:, :] = M[uvp, :]
    M[:, :] = M[:, uvp]
    isingmat = numpy.triu(M) + numpy.tril(M, -1).transpose()
    rel = Function.from_relation(relmap)
    h = list(isingmat.diagonal())
    J = {x: isingmat[x] for x in combinations(range(len(h)), 2)}
    isingmodel = IsingModel(rel, h, J, data["en0"])
    isingmodel.verify()
    crel = rel
    return crel, isingmodel
