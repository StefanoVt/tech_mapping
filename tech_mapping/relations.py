from functools import reduce
from itertools import product
import numpy
from canonicalize import FunctionCertificate
from pyrbc.nodes.base import RBCEdge, RBCNode
import pyrbc.rbc as rbc
from pyrbc.fasttruthtable import truth_table as truthtable
from pyrbc.rbc import var
import networkx as nx
from networkx.algorithms.isomorphism import GraphMatcher
from typing import List, Tuple, Dict, Union

def mapl(f, l):
    return list(map(f, l))

class Function(object):
    """A class representing a Boolean Function"""
    _all_canons = dict()
    def __init__(self, circuit, inputs):
        # type: (RBCEdge, Union[List[RBCNode],List[str]]) -> None
        """Boolean function from  RBC edge and list of input nodes"""
        self.aig = circuit

        if not isinstance(inputs[0], RBCNode):
            inputs = (mapl(lambda s: var(s).node, inputs))
        self.inputs = inputs

        cert = FunctionCertificate(len(inputs), truthtable(circuit, inputs))
        self.cert = cert
        if cert not in self._all_canons:
            self._all_canons[cert] = self

    def find_canonical(self):
        """Get the canonical function of the NPN equivalence class"""
        # type: () -> Function
        return self._all_canons[self.cert]

    def find_matching(self, other):
        """Given another function of the same NPN class, find the mapping between IO vars"""
        # type: (Function) -> Tuple[bool, Dict[RBCNode, RBCEdge]]

        assert self.get_key() == other.get_key()

        def make_graph(truthtable):
            ret = nx.Graph()
            for i in range(len(truthtable[0])):
                ret.add_edge((i, 0), (i,1))
                ret.node[i,0]["color"] = (i == 0)
                ret.node[i, 1]["color"] = (i == 0)

            for line in truthtable:
                for i,inp_ in enumerate(line):
                    ret.add_edge((i, inp_), (-1, tuple(line)))
                ret.node[-1,tuple(line)]["color"] = -1
            return ret
        g1 = make_graph(self.get_truth_table())
        g2 = make_graph(other.get_truth_table())

        def compare_nodes(n1, n2):
            return n1["color"] == n2["color"]

        ret = GraphMatcher(g1, g2, node_match=compare_nodes)

        def other_input(inp):
            assert inp  in self.inputs
            i = self.inputs.index(inp)
            newi, flip = ret.mapping[i+1,1]
            assert  newi
            return (inp, RBCEdge(other.inputs[newi-1], flip == 0))


        return ret.is_isomorphic() and (ret.mapping[0,1][1] == 0, dict(map(other_input,self.inputs)))


    def get_key(self):
        """Return an opaque object representing the NPN class (the certificate from nauty)"""
        # type: () -> FunctionCertificate
        return self.cert

    def num_vars(self):
        """Get the number of input and output variables"""
        # type: () -> int
        return len(self.inputs) + 1

    def get_truth_table(self):
        """Get the truth table of the function. Each row consist in the output + the inputs in order.
        Uses binary 0-1 values """
        # type: () -> List[List[int]]
        def tt(n):
            return truthtable(n, self.inputs)
        inputedges = [RBCEdge(x, False) for x in  self.inputs]
        return [[int(out)] + mapl(int, inps) for inps, out in zip(zip(*mapl(tt, inputedges)), tt(self.aig))]


    def permute(self, neworder):
        """Returns a permutation on the inputs. Names inside var nodes are not changed"""
        #type: () -> Function
        return Function(self.aig, [self.inputs[i] for i in neworder])

    def toggle(self, n):
        """Return the same function with a negated input"""
        # type: () -> Function
        if n:
            asmap = {i: RBCEdge(i, self.inputs.index(i) == n-1) for i in self.inputs}
            return Function(self.aig.symbolic_eval(asmap), self.inputs)
        else:
            return Function(~self.aig, self.inputs)

    @classmethod
    def from_relation(cls, relmap):
        """Get boolean function from a truth table"""
        # type: (numpy.ndarray) -> Function
        # assert order is [output, inputs]
        reldict = dict()
        numimps = relmap.shape[1] -1
        inputforms = list(map(rbc.var, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[:numimps]))
        ret = rbc.false()
        for row in relmap:
            inputs = row[1:]
            output = row[0]
            if output:
                ret = rbc.Or(ret, reduce(rbc.And, [v if pol else ~v for pol,v in zip(inputs, inputforms)]))

        inputnodes = [x.node for x in inputforms]
        return cls(ret, inputnodes)







# XXX: rewrite relation as a function with an implicit output, otherwise i cannot use canonicalize
class Relation(object):
    """Deprecated class do not use"""
    def __init__(self, relmat):
        raise RuntimeWarning("deprecated")
        self.relmat = relmat
        self.sort()

    def get_key(self):
        """Convert relation into unique representation

        it may be a good idea to call this method __hash__ and use directly Relation objects in other places"""
        return self.relmat.tobytes()

    @classmethod
    def from_aig(clazz, aig, inputs):
        """Create a Relation object from an RBC

        misleading name, can be used on any RBC node (Tech mapping is usually done on AIG though)"""
        models = []
        for ass in product(*([(False, True)] * len(inputs))):
            out = RBCEdge(aig, False).eval(dict(zip(inputs, ass)))
            models.append((out,) + (ass))
        relation = numpy.array(models)
        order = [aig] + inputs
        return clazz(relation), order

    def sort(self):
        """Sort row in the matrix to avoid multiple representations of the same relation"""
        self.relmat = numpy.array(sorted(self.relmat.tolist()))

    def num_vars(self):
        """Number of variables in the relation"""
        return self.relmat.shape[1]


    def find_canonical(self):
        raise NotImplementedError()

    def permute(self, order):
        self.relmat[:, :] = self.relmat[:, order]
        self.sort()

    def toggle(self, n):
        self.relmat[:, n] = 1 - self.relmat[:, n]
        self.sort()