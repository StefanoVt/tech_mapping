"""Greedy top-down mapper

This mapper greedily choses a gate starting from the outputs. Not optimal by a long shot, but works relatively well on
small circuits"""
from kcuts.kcuts import all_cuts2 as all_cuts
from pyrbc.graph import build_graph
from pyrbc.nodes.var import RBCVarNode
from .mapper import Mapper


class TopDownMapper(Mapper):
    def mapping_candidates(self, current, v, cutoff=20):
        candidates = self.candidates(v)
        candidates.sort(key=lambda x: -x.cutsize)
        candidates2 = []
        bestres = 1.0E100
        for candidate in candidates:
            if (current.size + candidate.area) > bestres:
                break
            candidate = current.add_gate(candidate)
            candidate.place_route()
            if candidate.size > 0:
                candidates2.append(candidate)
                bestres = min(bestres, candidate.size)
        return candidates2

    def empty_candidate(self, vv):
        ret = self.embeddingType.new_empty(self.topology)
        ret.set_outputs(vv)
        return ret

    def mapping(self, vv, cutoff=10):
        self.graph = build_graph(vv)
        candidates = [self.empty_candidate(vv)]
        while len(candidates):
            candidate = candidates.pop(0)
            if all(isinstance(x, RBCVarNode) for x in candidate.inputs):
                return candidate
            x = next(x for x in candidate.inputs if not isinstance(x, RBCVarNode))
            im = self.mapping_candidates(candidate, x)
            if not im:
                continue
            if len(candidates) < cutoff:
                candidates.extend(im)
                candidates.sort(key=lambda x: float(x.completion_heuristic[0]) / x.completion_heuristic[1])
                candidates.reverse()
                candidates = candidates[:cutoff]
