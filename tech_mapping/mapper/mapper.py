"""Mapper base class"""
from kcuts.kcuts import all_cuts, validate_cut
import networkx as nx
from pyrbc.nodes.var import RBCVarNode


class Mapper(object):
    """Mapper object, common base class"""
    def __init__(self, matcher, embeddingType, topology):
        """Constructor.

        matcher: GateDatabase object
        embeddingType: Embedding object class
        topology: hardware topology"""
        self.matcher = matcher
        self.embeddingType = embeddingType
        self.topology = topology
        self.k = 5
        self.graph = None
        self.cutsmemo = dict()

    def candidates(self, node):
        if isinstance(node, RBCVarNode):
            return []

        candidates = []

        for c in all_cuts(self.graph, node, self.k, self.cutsmemo):
            if node in c: continue
            gm = self.matcher.find_gate(node, c)
            if not gm: continue
            cutnodes = validate_cut(self.graph, gm.cut.output.node, gm.cut.inputs)
            gm.cut.set_cutnodes(cutnodes) # XXX should not be done here
            candidates.append(gm)
        return candidates

    def mapping(self, formula):
        """Return a Embedding object for the formula"""
        raise NotImplementedError()
