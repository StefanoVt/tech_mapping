"""Bottom-up mapper heuristics

Standard tech mapping implementation. Using area reduction is comparable to ABC, but using more expensive
heuristics is impractical"""

from .mapper import Mapper
from pyrbc.nodes.var import RBCVarNode
from pyrbc.graph import build_graph


class BottomUpMapper(Mapper):
    def mapping(self, vv):
        visited = set()
        bestcut = dict()

        def get_cover(x):
            if x is None:
                return {}
            ret = {x}
            for i in (x.cut.inputs):
                assert i in bestcut, (x.cut.output, x.cut.inputs, i, (bestcut))
                ret.update(get_cover(bestcut[i]))
            return ret

        def cut_cost(x):
            return sum(y.area for y in get_cover(x))

        self.graph = build_graph(vv)

        for v in vv:
            for n in v.iter_nodes(visited):
                if isinstance(n, RBCVarNode):
                    bestcut[n] = None
                    continue
                ncuts = self.candidates(n)
                bcut = min(ncuts, key=cut_cost)
                bestcut[n] = bcut


        ret = self.embeddingType([], {})
        ret.set_outputs(vv)
        repres = set()
        for v in vv:
            repres.update(get_cover(bestcut[v.node]))
        for n in repres:
            newvals = {}
            newq = len(ret.vars)
            for val in [n.cut.output.node] + n.cut.inputs:
                if val in ret.embedding:
                    newvals[val] = ret.embedding[val]
                else:
                    newvals[val] = newq
                    newq += 1

            ret = ret.add_gate(n, newvals)
        return ret


