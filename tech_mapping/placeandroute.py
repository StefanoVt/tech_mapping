from minorminer import find_embedding
from pyomo.environ import *

class PlaceAndRoute(object):
    def __init__(self, topology):
        self.topology = topology
        self.embedding = []
        self.completion_heuristic = 0

    def place_route(self, embedding):
        """Use Solver API global embedding to do place&route"""
        relmat = embedding.unconstrained_ising()
        relmat = relmat.tocoo()
        edgelist = zip(relmat.row, relmat.col)
        self.embedding = find_embedding(edgelist, self.topology)
        if self.embedding:
            self.completion_heuristic = (
                len(set(n for g in embedding.gates for n in g.cutnodes)) + len(embedding.inputs), self.size)

    @property
    def size(self):
        """Total size of the embedding"""
        if not self.embedding:
            return 0
        return sum(len(x) for x in self.embedding)