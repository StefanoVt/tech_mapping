"""Classes modeling single Chimera gates/constraints"""
from itertools import product

import numpy
import typing
from typing import Tuple, Dict, List
import copy
from tech_mapping.relations import Function


class IsingModel(object):
    """Class representing a penalty Ising model for a relation. It is mutable in order to allow NPN transformations"""
    def __init__(self, relation, h, J,  isingoff):
        # type: (Function, List[float], Dict[Tuple[int,int], float], float) -> None
        self.biases = h
        self.couplings = J
        self.offset = isingoff
        self.relation = relation
        #self.varnames = "ABCDEF"[:self.num_qubits()]

    def permute(self, order):
        """Switch variables into a new order

        order is a permutation array, does not permute ancillas"""
        assert len(order) == self.relation.num_vars()
        assert order[0] == 0, "output permutation not supported here"
        self.relation = self.relation.permute([x-1 for x in order[1:]])
        #self.varnames = [self.varnames[i] for i in order]
        order = order + [x for x in range(self.num_qubits()) if x not in order]
        newh = [self.biases[x] for x in order]
        newJ = {}
        for k, v in self.couplings.items():
            a,b = order.index(k[0]), order.index(k[1])
            a,b = min(a,b), max(a,b)
            newJ[a,b] = v
        self.couplings = newJ
        self.biases = newh

    def toggle(self, n):
        """Toggle polarity of a variable

        n is the index of the variable"""
        self.relation = self.relation.toggle(n)
        self.biases[n] *= -1
        for k in self.couplings:
            if n in k:
                self.couplings[k] *= -1


    def num_qubits(self):
        """number of qubits in the model"""
        return len(self.biases)

    @property
    def num_ancilla(self):
        """number of ancilla in the models"""
        return self.num_qubits() - self.relation.num_vars()

    def verify(self):
        """verify correctness by enumerating possible values
        """
        h = numpy.array(self.biases)
        J = numpy.zeros((self.num_qubits(), self.num_qubits()), dtype=numpy.float)
        for a,b in self.couplings:
            J[a,b] = self.couplings[a,b]
        R = self.relation.get_truth_table()
        off = self.offset
        nancilla = self.num_ancilla

        def value(q, a):
            qbs = numpy.concatenate((q, a))
            return (h.transpose().dot(qbs)) + qbs.transpose().dot(J.dot(qbs)) + off

        for xvals in product(*([(0,1)] * self.relation.num_vars())):
            qubits = numpy.array(xvals) * 2 - 1
            if self.num_ancilla == 0:
                allancillas = [[]]
            else:
                allancillas = list(product(*([(-1, 1)] * nancilla)))
            cvalues = [value(qubits, a) for a in allancillas]
            if list(xvals) in R:
                assert all(-1e-3 <= v for v in cvalues), (cvalues, qubits)
                assert any(v < 1e-3 for v in cvalues)
            else:
                assert all(v >= 0.999 for v in cvalues), (cvalues, qubits, xvals, R)


    def match(self, rel):
        # type: (Function) -> IsingModel
        ret= self.__class__(self.relation, self.biases,self.couplings, self.offset)
        flip, imap = rel.find_matching(self.relation)
        neworder = [0]+[self.relation.inputs.index(imap[i].node)+1 for i in rel.inputs]
        ret.permute(neworder)
        for i in rel.inputs:
            if imap[i].flip:
                inpnames = [x for x in ret.relation.inputs]
                ret.toggle(1 + inpnames.index(imap[i].node))
        if flip:
            ret.toggle(0)

        return ret


class IsingModelWrapper(object):
    """Wrapper for Ising model that allows to do Boolean matching explicitly only when needed"""
    def __init__(self, model, function):
        # type: (IsingModel, Function) -> None
        self.canonical = model
        self.function = function
        self._sorted = None

    def get(self):
        """Get correct ising model for the function from NPN canonical"""
        # type: () -> IsingModel
        if self._sorted is None:
            self._sorted = self.canonical.match(self.function)
        return self._sorted

    def num_qubits(self):
        return self.canonical.num_qubits()

    #@property
    #def varnames(self):
    #    return self.canonical.varnames