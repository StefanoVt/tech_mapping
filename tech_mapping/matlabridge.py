import os
import os.path
from json import loads, dumps

import matlab.engine
import matlab.mlarray
import numpy
from dwave_sapi2.util import get_chimera_adjacency
from pyrbc.nodes.var import RBCVarNode
from scipy.sparse import coo_matrix

from tech_mapping.relations import Relation
from tech_mapping.embedding import GateNetwork
from tech_mapping.matching import GateMatch
from tech_mapping.gate_db import GateDatabase

mlarray = matlab.mlarray
engine = None

def matlab_call(eng, func, para):
    return loads(eng.pythonBridge(func, dumps(para)))


def matlabEngine():
    global engine
    if engine is None:
        engine = matlab.engine.start_matlab()
        olddir = os.getcwd()
        engine.cd(os.getenv("HOME"), nargout=0)
        engine.matlabrc(nargout=0)
        engine.cd(olddir, nargout=0)
    return engine


class MatlabRelation(Relation):
    """Object representing a relation between booleans

    Essentially a numpy matrix plus utility functions"""
    def find_canonical(self):
        """Find the canonical relation under PN equivalence

        Calls matlab code for now. Returns the new relation plus the PN operations"""
        eng = matlabEngine()
        crel, negVars, neworder = eng.canonicalRelationWithNegation(mlarray.int8(self.relmat.tolist()), nargout=3)
        neworder = map(lambda x: int(x) - 1, neworder._data)
        negVars = map(bool, negVars._data)
        canonical_relation = numpy.array(crel, dtype=numpy.int_)
        return MatlabRelation(canonical_relation), neworder, negVars

class GateDatabaseMatlab(GateDatabase):
    """Library of gates"""

    def find_gate(self, cut):
        """Check if a gate exists s.t. is NPN equivalent to a cut"""
        rel, orderv = MatlabRelation.from_aig(*cut.to_circuit())
        rel, order, negvars = rel.find_canonical()
        if rel.get_key() not in self.database:
            return None
        else:
            origmodel = self.database[rel.get_key()]
            isingmodel = origmodel.from_canonical(order, negvars)
            ret = GateMatch(cut, isingmodel, orderv)
            # ret.verify()
            return ret

class SimpleGateDatabase(GateDatabase):
    """Simple database that maps AND2 nodes to qubit triangles (thus has to use global embedding)"""
    def __init__(self):
        GateDatabase.__init__(self, None)

    def all_gates(self, v):
        ret = []
        isingmat = numpy.array([
            [-0.5, 0.5, -1],
            [0, -1, -1.5],
            [0, 0, 1.5]
        ])
        isingoffset = 2
        if v.children:
            relnodes = [x.node for x in v.children] + [v, ]
            relmat = numpy.array([[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 1]])

            rel = MatlabRelation(relmat)
            isingmodel = IsingModel("and2", rel, isingmat, isingoffset)
            if v.left.flip:
                isingmodel.toggle(0)
            if v.right.flip:
                isingmodel.toggle(1)
            ret.append(GateMatch(KCut(v, [x.node for x in v.children]), isingmodel, relnodes))
        return ret



class EmbeddingMatlab(GateNetwork):
    """Embedding class that uses the matlab CSP compier to perform place&route"""
    _base_bridge = None

    def place_route(self):
        eng = matlabEngine()

        # uncomment to save the bridge object state when debugging
        # eng.workspace["bridge"] = self.handle
        # eng.save("test.mat", "bridge", nargout=0)

        self.handle = eng.searchEmbedding(self.handle)
        # embedding is passed as JSON data, data conversion between py an matlab is limited
        rawembedding = loads(eng.getEmbedding(self.handle))
        if rawembedding["root"]:
            self.embedding = map(lambda x: [x - 1] if type(x) == int else map(lambda y: y - 1, x), rawembedding["root"])
            self.completion_heuristic = (
                len(set(n for g in self.gates for n in g.cutnodes)) + len(self.inputs), self.size)
        else:
            self.embedding = []

    def add_constraint(self, constr):
        """Add constraint to the bridge matlab object"""
        eng = matlabEngine()
        name, vars = constr.to_matlab_constraint(self.vars)
        self.handle = eng.addConstraint(self.handle, name, mlarray.double(vars))

    def __init__(self, handle, topology, topologysize, outputs, inputs, gates):
        """Constructor.

        CAUTION: topology here is provided as an int, just to avoid sending big matrices back and forth to matlab.
        Can probably be avoided in some cleaner way"""
        GateNetwork.__init__(self, topology, outputs, inputs, gates)
        self.topologysize = topologysize
        self.handle = handle

    def add_gate(self, newgate):
        """Add a new gate to the embedding

        NB: different constructor and topology attribute from superclass"""
        inputs = (self.inputs - newgate.outputs) | (newgate.inputs - set(self.vars))
        ret = self.__class__(self.handle, self.topology, self.topologysize, self.outputs, frozenset(inputs),
                             self.gates + (newgate,))
        ret.add_constraint(newgate)
        return ret

    @classmethod
    def create_relmap(cls, fn):
        """Create and initialize the bridge matlab object.

        fn is the filename of the GENLIB library file."""
        eng = matlabEngine()
        cls._base_bridge = eng.loadRelmap(eng.CSPBridge(), fn)

    @classmethod
    def new_empty(cls, topology):
        """Create a new empty embedding"""
        eng = matlabEngine()
        handle = eng.chooseTopology(cls._base_bridge, topology)
        return cls(handle, get_chimera_adjacency(topology), topology, tuple(), tuple(), tuple())


    def show_embedding(self):
        """Show the embedding using matlab and the embeddingView executable"""
        eng = matlabEngine()
        top = coo_matrix(([1] * len(self.topology), zip(*list(self.topology))))
        assert all(top.toarray()[i, j] == 1 for i, j in self.topology)
        Q = mlarray.double(self.unconstrained_ising().toarray().tolist())
        A = mlarray.double(top.toarray().tolist())
        emb = map(mlarray.double, [[x + 1 for x in y] for y in self.embedding])
        eng.visualizeEmbedding(Q, A, emb, nargout=0)



class Embedding2(EmbeddingMatlab):
    """This class implement additional methods used in bottom-up mapping that I still don't know how to refactor"""
    def embed_all(self, nodes):
        """embedd all nodes in a single step"""
        inputs = set()
        vars = set()

        def recgates(nodes):
            ret = [x.bestcuts[0].match for x in nodes]
            vars.update(nodes)
            for m in ret:
                for y in m.inputs:
                    if isinstance(y, RBCVarNode):
                        inputs.add(y)
                    elif y not in vars:
                        ret.extend(recgates([y]))
            return ret

        gates = tuple(recgates(nodes))

        ret = self

        for gate in gates:
            ret = ret.add_gate(gate)
        # ret.set_outputs(nodes)

        return ret

    def place_route2(self, nodes):
        """Place and route all nodes"""
        ret = self.embed_all(nodes)

        ret.place_route()
        return ret
        # cut.embedding = ret
        # return cut.embedding

    def active(self):
        """unused ?"""
        buff = [x.node for x in self.outputs]
        ret = list()
        while len(buff):
            r = buff.pop(0)
            if isinstance(r, RBCVarNode): continue
            buff.extend(r.bestcuts[0].inputs)
            if r not in ret:
                ret.append(r)
        ret.reverse()
        return ret

    def placement_score(self, nodes):
        """Try calling placement matlab code to get a score"""
        ret = self.embed_all(nodes)
        eng = matlabEngine()
        score = eng.tryPlacement(ret.handle)
        return score

