"""Classes for embeddings of AIGs"""
from itertools import product

import numpy
from collections import defaultdict
from minorminer import find_embedding
from dwave_embedding_utilities import embed_ising, unembed_samples, discard
from itertools import combinations
from typing import List,Set, Dict, Any,Optional, Tuple
from pyrbc.nodes.base import RBCNode, RBCEdge
from pyrbc.nodes.var import RBCVarNode
from tech_mapping.matching import GateMatch
from scipy.sparse import lil_matrix as spmat


class EmbeddingTopology(object):
    def __init__(self, topology, network):
        self.topology = topology
        self.net = network

    def get_embedding(self):
        uising = self.net.unconstrained_ising()
        self.uising = uising
        edges = uising[1].keys()
        self.emb =  find_embedding(edges, self.topology.edges())

    def expand_embedding(self):
        self.get_embedding()
        h,J,off = self.uising
        emb = self.emb
        v = len(self.net.vars)
        bigh, bigJ, Jp = embed_ising(dict(enumerate(h)),J,
                           emb, self.topology)
        bigJ.update(Jp)
        return bigh, bigJ, off- sum(Jp.values())

    def unembed_solution(self, solution):
        return unembed_samples(solution, self.emb)



class GateNetwork(object):

    def __init__(self, gates, embedding, io_skip=None):
        # type: (List[GateMatch], Dict[RBCNode, Any], Optional[Tuple[Set[RBCNode],List[RBCNode]]]) -> None
        self.gates = gates
        self.embedding = embedding
        self.outputs = []
        if io_skip is None:
            self._update_inps_vars()
        else:
            self.inputs, self.vars = io_skip

    def _update_inps_vars(self):
        outputs = set(x.cut.output.node for x in self.gates)
        inputs = set()
        vars = []

        for x in self.gates:
            inputs.update(x.cut.inputs)
            if x.cut.output.node not in vars:
                vars.append(x.cut.output.node)
            for y in x.cut.inputs:
                if y not in vars:
                    vars.append(y)

        self.inputs = inputs.difference(outputs)
        self.vars = vars

    def add_gate(self, newgate, qbitmap):
        # type: (GateMatch, Dict[RBCNode,Any]) -> GateNetwork
        """Add a new gate to the embedding

        Embeddigs are (should be) immutable, so a new embedding is returned
        """
        inputs = (self.inputs - {newgate.cut.output.node}) | (set(newgate.cut.inputs) - set(self.embedding))
        newemb = dict(self.embedding)
        assert (x not in newemb or newemb[x] == qbitmap[x] for x in qbitmap)
        newemb.update(qbitmap)
        vars = list(self.vars)
        if newgate.cut.output.node not in vars:
            vars.append(newgate.cut.output.node)
        for x in newgate.cut.inputs:
            if x not in vars:
                vars.append(x)
        ret = self.__class__(self.gates + [newgate], newemb, (inputs, vars))
        ret.set_outputs(self.outputs)
        return ret

    def set_outputs(self, o):
        #type: (List[RBCEdge]) -> None
        self.outputs = o

    @property
    def size(self):
        return sum(g.area for g in self.gates)

    def unconstrained_ising(self):
        """Return the unembedded version of the Ising model.

        All gate ising models are summed together.
        """
        vars = self.vars
        nancilla = sum(x.ising_model.num_ancilla for x in self.gates)
        ancillas = range(len(vars), len(vars) + nancilla)
        nvars = len(vars) + nancilla
        #relmat = spmat((nvars, nvars))
        h = [0]*nvars
        J = defaultdict(float)
        off = 0
        for gate in self.gates:
            smodel = gate.ising_model
            indexes = [vars.index(gate.cut.output.node)] + map(lambda x: vars.index(x), gate.cut.inputs)
            for _ in range(smodel.num_ancilla):
                indexes.append(ancillas.pop())
            assert len(indexes) == smodel.num_qubits()

            for i,val in zip(indexes, smodel.biases):
                h[i] += val

            for (i,j), v in smodel.couplings.items():
                a,b = indexes[i], indexes[j]
                if a > b:
                    a, b = b,a
                J[a,b] += v

            off += smodel.offset
            #for i_, i in enumerate(indexes):
            #    for j_, j in enumerate(indexes):
            #        i,j = min(i,j), max(i,j)
            #        relmat[i, j] += smodel.isingmat[i_, j_]
        return (h,J,off)

    def unconstrained_ising_offset(self):
        """Return the offset of the Ising model."""
        return sum(x.ising_model.isingoffset for x in self.gates)



    def get_blif(self):
        """Return gates in BLIF format

        CAUTION: not gates are not printed (polarity is ignored), see GateMatch.get_blif_gate()"""
        ret = ""
        ret += ".inputs " + ' '.join("n" + str(self.vars.index(i)) for i in self.inputs) + "\n"
        ret += ".outputs " + " ".join("n" + str(self.vars.index(o.node)) for o in self.outputs) + "\n"
        for gate in reversed(self.gates):
            ret += gate.get_blif_gate(self.vars)
        return ret

    def verify(self):
        """Verify that the ising model matches with the AIG

        Simplification: only counter-models with a flipped output are checked"""
        output = self.vars.index(next(iter(self.outputs)).node)
        h,Jd,off = self.unconstrained_ising()
        h = numpy.array(h)
        J = numpy.zeros(shape=(len(h),len(h)), dtype=numpy.float)
        for k,v in Jd.iteritems():
            J[k] += v

        def value(q, a):
            qbs = numpy.concatenate((q, a))
            return (h.transpose().dot(qbs)) + qbs.transpose().dot(J.dot(qbs)) + off

        assert all(isinstance(x, RBCVarNode) for x in self.inputs), self.inputs  # type: List[RBCVarNode]
        vars = [x.name for x in self.inputs]
        for assgn in product(*([(False, True)] * len(vars))):
            assgndict = dict(zip(vars, assgn))
            qubits = numpy.array([1 if v.eval(assgndict) else -1 for v in self.vars])
            nancilla = h.size - qubits.size
            if h.size == qubits.size:  # no ancilla
                allancillas = [[]]
            else:
                allancillas = product(*([(-1, 1)] * nancilla))
            #zerofound = False
            en0 = min(value(qubits, a) for a in allancillas)
            assert -1e-2 <= en0 <= 1e-3
            qubits[output] *= -1
            assert all(value(qubits, a) >=0.999 for a in allancillas)
            qubits[output] *= -1
            #    assert -1.0e-3 <= v
            #    zerofound |= v <= 1.0e-3
            #    qubits[output] *= -1
            #    v = value(qubits, a)
            #    assert 0.999 <= v, (v, qubits, a)
            #    qubits[output] *= -1
            #assert zerofound