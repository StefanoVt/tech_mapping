from pyrbc.nodes.base import RBCNode, RBCEdge

from tech_mapping.matching import GateMatch, Cut
from tech_mapping.relations import Function



class GateDatabase(object):
    """Database of Ising models, finds a model for a certain NPN class"""
    def __init__(self, db):
        # type: (Dict[FunctionCertificate, IsingModel]) -> None
        self.database = db

    def find_gate(self, output, inputs):
        # type: (RBCEdge, List[RBCNode]) -> Optional[GateMatch]
        """Check if a gate exists s.t. is NPN equivalent to a cut"""
        if isinstance(output, RBCNode):
            output = RBCEdge(output, False)
        inputs = list(inputs)
        rel = Function(output, inputs)
        if rel.get_key() in self.database:
            model = self.database[rel.get_key()]
            return GateMatch(Cut(output, inputs), model, rel)
        else:
            return None